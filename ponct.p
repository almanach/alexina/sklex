@ponctuation
< pred_vide
;
@ponctuation_faible
#{
  < @ponctuation
  < ponctw
#|
#  < @changement_possible_de_phrase
#}
;
@virgule
< ponctw
{
  < pred=__virgule<arg1,arg2>
  < form_virgule
|
  < pred=0
}
;
@ponctuation_forte
#{
  < @ponctuation
  < poncts
#|
#  < @changement_possible_de_phrase
#}
;
@ponctuation_forte_ou_epsilon
{
  < @epsilon
|
  < @ponctuation
  < ponctw
#|
#  < @changement_possible_de_phrase
}
;
@changement_possible_de_phrase
< pred_vide
< sbound
< weight=10
;
@epsilon
< epsilon
;
.
