#!/usr/bin/env perl

$cat = shift;

$cat=~s/aaa-//;

$cat=~s/snc-//;
$cat=~s/^A$/adj/;
$cat=~s/^V$/v/;
$cat=~s/^S$/nc/;
$cat=~s/^D2?$/adv/;
$cat=~s/^ND$/adv-num/;
$cat=~s/^NA$/adj/;
$cat=~s/^E$/prep/;
$cat=~s/^G$/adj/;
$cat=~s/^J$/pres/;
$cat=~s/^R$/sa/;
$cat=~s/^PF$/det/;
$cat=~s/^PP$/pro/;
$cat=~s/^PD$/pro/;
$cat=~s/^PU$/pro/;
$cat=~s/^PA$/pro-ind/;
$cat=~s/^Ocsu$/csu/;
$cat=~s/^Ocoo$/coo/;
$cat=~s/^OY$/csu-by/;
$cat=~s/^T$/adv/;
$cat=~s/^0$/num/;
$cat=~s/^Y$/by/;

while (<>) {
    chomp;
    next if (/<error>/ || /\t\?\t/ || /^\t*$/);
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t.*$/ || die ("error:$_\n");
    $form=$1;
    $lemma=$2;
    $tag=$4;
    $catext="";
    if ($tag ne "") {$catext="-".$tag;}
    $line="$1\t\t$cat$catext\t[pred=\'$lemma\', cat = $cat";
    if ($tag ne "") {
	$line.=", \@$tag]\n";
    } else {
	$line.="]\n";
    }
    if ($tag=~/\-\-[sp]$/) {
	$line_orig=$line;
	$line=~s/\-(\-[sp][\t\]])/-ma\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-(\-[sp][\t\]])/-mi\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-(\-[sp][\t\]])/-f\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-(\-[sp][\t\]])/-n\1/g;
	print $line;
    } elsif ($tag=~/\-m\-[sp]$/) {
	$line_orig=$line;
	$line=~s/\-m(\-[sp][\t\]])/-ma\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-m(\-[sp][\t\]])/-mi\1/g;
	print $line;
    } elsif ($tag=~/\-a\-[sp]$/) {
	$line_orig=$line;
	$line=~s/\-a(\-[sp][\t\]])/-ma\1/g;
	print $line;
    } elsif ($tag=~/\-i\-[sp]$/) {
	$line_orig=$line;
	$line=~s/\-i(\-[sp][\t\]])/-mi\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-i(\-[sp][\t\]])/-f\1/g;
	print $line;
        $line=$line_orig;
	$line=~s/\-i(\-[sp][\t\]])/-n\1/g;
	print $line;
    } else {
	print $line;
    }
}
